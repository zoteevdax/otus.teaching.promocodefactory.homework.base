﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IList<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data.ToList();
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data.AsEnumerable());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<bool> IsExistAsync(Guid id)
        {
            return Task.FromResult(Data.Any(x => x.Id == id));
        }

        public Task<Guid> AddAsync(T entity)
        {
            var newGuid = Guid.NewGuid();
            entity.Id = newGuid;

            Data.Add(entity);
            
            return Task.FromResult(newGuid);
        }

        public Task UpdateAsync(T entity)
        {
            var entityInRepo = Data.FirstOrDefault(x => x.Id == entity.Id);

            if (entityInRepo == null)
                throw new InvalidOperationException();

            var entityKey = Data.IndexOf(entityInRepo);
            Data[entityKey] = entity;

            return Task.CompletedTask;
        }

        public Task DeleteAsync(Guid id)
        {
            var entityInRepo = Data.FirstOrDefault(x => x.Id == id);

            if (entityInRepo == null)
                throw new InvalidOperationException();

            Data.Remove(entityInRepo);

            return Task.CompletedTask;
        }
    }
}