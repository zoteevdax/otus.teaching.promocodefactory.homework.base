﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Extensions
{
    public static class EmployeeMapper
    {
        public static Employee ToEmployee(this EmployeeRequest employeeRequest, List<Role> roles)
        {
            return employeeRequest.ToEmployee(Guid.Empty, roles);
        }

        public static Employee ToEmployee(this EmployeeRequest employeeRequest, Guid id, List<Role> roles)
        {
            return new Employee
            {
                Id = id,
                FirstName = employeeRequest.FirstName,
                LastName = employeeRequest.LastName,
                AppliedPromocodesCount = employeeRequest.AppliedPromocodesCount,
                Email = employeeRequest.Email,
                Roles = roles ?? new List<Role>()
            };
        }
    }
}
